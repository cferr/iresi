/* This file is part of CMSCD, Count-Min Sketch and CoDeviance
 * Calculator.
 * 
 * CMSCD is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * CMSCD is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with CMSCD.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CMSCD_H
#define CMSCD_H
#include "datahandler.h"
#include "cms.h"
#include "utils.h"
#include "codeviance.h"
#include "debug.h"
#include "exactanalysis.h"

double genericCodev2(packetStream* strm1, packetStream* strm2, int withCMS, 
    hashfunc** hFuncs, int numPackets, 
    int cmsTableLength, int numFunctions, int verbosity);

double** autobuildCodevMatrix(packetStream** streamList, int streamCount, 
    int packetCount, int useSketch, double delta, 
    double epsilon, int verbosity);

#endif