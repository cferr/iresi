/* This file is part of CMSCD, Count-Min Sketch and CoDeviance
 * Calculator.
 * 
 * CMSCD is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * CMSCD is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with CMSCD.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "utils.h"

/*
 * Returns the minimum of a and b.
 */

int min(int a, int b) {
    if(a < b)
        return a;
    return b;
}

/*
 * Prints errors to stderr with a standard format.
 */
void printErr(char* progname, char* str)
{
    fprintf(stderr, "%s: %s\n", progname, str);
}

/*
 * Prints an int matrix with a pretty format.
 */
void printIntMatrix(int** mat, int x, int y)
{
    for(int i = 0; i < y; i++)
    {
        printf("[");
        for(int j = 0; j < x; j++)
        {
            printf("%d", mat[i][j]);
            if(j < x-1)
                printf("\t");
        }
        printf("]\n");
    }
}

/*
 * Prints an unsigned char matrix with a pretty format.
 */
void printUCharMatrix(unsigned char** mat, int x, int y)
{
    for(int i = 0; i < y; i++)
    {
        printf("[");
        for(int j = 0; j < x; j++)
        {
            printf("%d", (int)mat[i][j]);
            if(j < x-1)
                printf("\t");
        }
        printf("]\n");
    }
}

/*
 * Does the same as getline, but with no limitation on length.
 */
char* getuline(FILE* handler, int* length)
{
    int pos = 0;
    char c;
    
    while((c = fgetc(handler)) != '\n') {
        if(c == EOF) {
            *length = 0;
            return NULL;
        }
        pos++;
    }
    
    fseek(handler, (long)(-(pos + 1)), SEEK_CUR);
    
    char* retStr = (char*)calloc(pos + 1, sizeof(char));
    char* status = fgets(retStr, pos + 1, handler);
 
    // status is either null or retStr
    if(status == NULL) {
        free(retStr);        
        *length = 0;
        return NULL;
    }
    
    // pass by the line break
    c = fgetc(handler); 
    
    *length = pos;
    return retStr;
}