#ifndef CMSCD_PARAMS_H
#define CMSCD_PARAMS_H
#include <stdlib.h>

/*
 * The different input sources available for CMSCD to use.
 */
typedef enum {
    file_analysis, 
    poisson_analysis, 
    gaussian_analysis, 
    zipfian_analysis, 
    binomial_analysis
} analysis_type;

/*
 * A structure to parse the program's arguments into usable parameters.
 */
typedef struct analysis_param {
    double double_val; 
    int int_val; 
    struct analysis_param *next;
} analysis_param;

analysis_param* addParam(analysis_param* existing, int intVal, double doubleVal);
void freeParamStructure(analysis_param* prm);


#endif