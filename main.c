/* This file is part of CMSCD, Count-Min Sketch and CoDeviance
 * Calculator.
 * 
 * CMSCD is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * CMSCD is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with CMSCD.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include <getopt.h>
#include "datahandler.h"
#include "cms.h"
#include "utils.h"
#include "codeviance.h"
#include "debug.h"
#include "cmscd.h"
#include "cmscd_params.h"

#define CMSCD_VERSION "0.3"

static int verbosity = 0; // the degree at which we will talk to the user
static int is_py = 0; // set to 1 if we output a python file
static int do_CMS = 0; // set to 1 if we calculate codev using CMS
static int do_exact = 0; // set to 1 if we calculate exact codev

/*
 * Shows a little help message to the user.
 */
void showHelp(char* path)
{
    printf("This is CMSCD %s.\n" \
    "Usage : %s [OPTION]... SOURCE1 SOURCE2 ... SOURCEn\n" \
    "Options : \n" \
    " -h, --help              Show this help.\n" \
    "     --silent            Only output the result.\n" \
    "     --verbose           Be verbose.\n" \
    "     --talkative         Output all the calculation results.\n" \
    "     --python            Output a Python script to display the result graphically.\n"
    "                         This option cancels any other output option.\n" \
    " -n, --number=[LIMIT]    Do not process more than [limit] packets per file.\n"\
    "                         Zero value means no limit. Defaults to 0.\n" \
    " -x, --exact             Calculate the exact codeviance matrix.\n"\
    " -c, --use-cms           Calculate estimated codeviance matrix using CMS.\n"\
    "CMS is calculated using a delta-epsilon approach. You can specify these "\
    "parameters here.\n"\
    " -d, --delta=[DELTA]     Set delta to [DELTA]. Defaults to 10^-3.\n" \
    " -e, --epsilon=[EPSILON] Set epsilon to [EPSILON]. Defaults to 2.10^-3.\n\n" \
    "You can add randomly generated sources to the input files. These switches\n" \
    "provide random sources with different probability sets.\n" \
    " -p, --poisson=[LAMBDA]  Use the Poisson law with parameter LAMBDA.\n" \
    " -b, --binomial=[N],[P]  Use the binomial law with parameters N,P.\n" \
    " -g, --gauss=[SIGMA]     Use the Gaussian law with parameter SIGMA.\n" \
    "You can add as many random switches as you want.\n" \
    , CMSCD_VERSION, path);
}

/*
 * Starter function for the program.
 */
int main(int argc, char* argv[])
{
    /* Analysis of the arguments */
    
    int numFiles = 0; // how many files we will have to analyze
    unsigned long packlimit = 0; // how many packets we will analyze at most
    
    double delta = 0.0001; // default value for CMS delta
    double epsilon = 0.0002; // default value for CMS epsilon
    
    int c; // Character to parse arguments
    
    /* This variable is defined in utils.h */
    progname = argv[0];
    
    // this allocation exceeds a litle bit what we really need, but anyway...
    analysis_type* analysis = (analysis_type*)calloc(argc, sizeof(int));
    analysis_param** analysis_params = 
        (analysis_param**)calloc(argc, sizeof(analysis_param));
    
    int artifDistCnt = 0;
    
    while (1)
    {
        static struct option long_options[] =
        {
            /* These options set a flag. */
            {"silent",   no_argument, &verbosity, 0},
            {"verbose",  no_argument, &verbosity, 1},
            {"talkative",no_argument, &verbosity, 2},
            {"python",   no_argument, &is_py,     1},
            /* These options don’t set a flag.
                We distinguish them by their indices. */
            {"number",  required_argument, 0, 'n'},
            {"delta",   required_argument, 0, 'd'},
            {"epsilon", required_argument, 0, 'e'},
            {"help",    no_argument,       0, 'h'},
            {"poisson", required_argument, 0, 'p'},
            {"gauss",   required_argument, 0, 'g'},
            {"zipf",    required_argument, 0, 'z'},
            {"binom",    required_argument, 0, 'b'},
            /* These options set flags, see below. */
            {"use-cms",  no_argument, 0, 'c'},
            {"exact",    no_argument, 0, 'x'},
            {0, 0, 0, 0}
        };
        /* getopt_long stores the option index here. */
        int option_index = 0;

        c = getopt_long (argc, argv, "cxhn:d:e:p:g:z:",
                    long_options, &option_index);

        /* Detect the end of the options. */
        if (c == -1)
            break;
        
        analysis_param* par = NULL;
        
        switch (c)
        {
            case 'h':
                showHelp(progname);
                return EXIT_SUCCESS;
                break;

            case 'd':
                if(optarg == NULL) {
                    printErr(progname, "No delta provided after --delta, exiting");
                    return -1;
                }
                delta = strtod(optarg, NULL);
                break;

            case 'e':
                if(optarg == NULL) {
                    printErr(progname, "No epsilon provided after --epsilon, exiting");
                    return -1;
                }
                epsilon = strtod(optarg, NULL);
                break;

            case 'n':
                if(optarg == NULL) {
                    printErr(progname, "No number provided after --number, exiting");
                    return -1;
                }
                packlimit = strtoul(optarg, NULL, 10);
                break;
            
            
            /* Random distributions below */
            case 'p': // Poisson
                if(optarg == NULL) {
                    printErr(progname, "No lambda provided after --poisson");
                    break;
                }
                analysis[artifDistCnt] = poisson_analysis;
                par =  addParam(NULL, 0, atof(optarg));
                analysis_params[artifDistCnt] = par;
                artifDistCnt++;
                break;
            
            case 'g': // Gauss
                if(optarg == NULL) {
                    printErr(progname, "No lambda provided after --gauss");
                    break;
                }
                analysis[artifDistCnt] = gaussian_analysis;
                par =  addParam(NULL, 0, atof(optarg));
                analysis_params[artifDistCnt] = par;
                artifDistCnt++;
                break;
                
            case 'b': // Binomial
                if(optarg == NULL) {
                    printErr(progname, "No lambda provided after --binom");
                    break;
                }
                
                char* cIndex = strstr(optarg, ",");
                if(cIndex)
                {
                    analysis[artifDistCnt] = binomial_analysis;
                    par = addParam(NULL, atoi(optarg), 0); //n
                    par = addParam(par, 0, atof(cIndex + 1)); //p
                    analysis_params[artifDistCnt] = par;
                    artifDistCnt++;
                }
                else printErr(progname, "Missing parameters for binom : --binom n,p");
                break;
                
            case 'z': // Zipf
                printf("Zipfian distribution not implmented yet.\n");
                break;
                
            case 'c':
                do_CMS = 1;
                break;
            
            case 'x':
                do_exact = 1;
                break;
                
            case '?':
                /* getopt_long already printed help. */
                break;

            default:
                break;
        }
    }
    if((do_CMS == 0 && do_exact == 0) || (artifDistCnt == 0 && argc == optind))
    {
        showHelp(progname);
        return EXIT_SUCCESS;
    }

    numFiles = argc - optind;
    char** fileList = malloc((size_t)numFiles * sizeof(char*));
    int curFile = 0;
    
    /* Create a list of the files to parse */
    for(int j = optind; j < argc; j++)
    {
        int nameLength = strlen(argv[j]);
        fileList[curFile] = (char*)malloc((nameLength) * sizeof(char));
        strncpy(fileList[curFile], argv[j], nameLength);
        curFile++;
    }
    
    if(verbosity >= 1)
        printf("This is CMSCD, Count-Min-Sketch and Codeviance.\n");
    
    /* Generate stream handlers for the successive streams */
    
    gsl_rng* gener = gsl_rng_alloc (gsl_rng_taus); //Tausworthe generator
    char* urandomint = getRandomBytes(1);
    gsl_rng_set (gener, urandomint[0]);
    free(urandomint);
    
    /* Allocate as much streams as we will need */
    packetStream** streams = (packetStream**)calloc(artifDistCnt + curFile,
                                                    sizeof(packetStream*));
    
    /* Build the random streams... */
    for(int i = 0; i < artifDistCnt; i++)
    {
        analysis_param* apar = analysis_params[i];
        double mu;
        double sigma;
        double p;
        int n;
        
        switch(analysis[i]) {
            case poisson_analysis:
                mu = apar->double_val;
                if(verbosity >= 2)
                    printf("Generating a Poisson stream of %ld packets...\n", packlimit);
                streams[i] = poissonStream(gener, packlimit, mu);
                break;
            case gaussian_analysis:
                sigma = apar->double_val;
                if(verbosity >= 2)
                    printf("Generating a Gaussian stream of %ld packets...\n", packlimit);
                streams[i] = gaussianStream(gener, packlimit, sigma);
                break;
            case binomial_analysis:
                p = apar->double_val;
                apar = apar->next;
                n = apar->int_val;
                if(verbosity >= 2)
                    printf("Generating a binomial stream of %ld packets...\n", packlimit);
                streams[i] = binomialStream(gener, packlimit, p, n);
                break;
            default:
                break;
        }
        freeParamStructure(analysis_params[i]);
    }
    
    /* ...then build the streams that use files. */
    
    int curStream = artifDistCnt;
    
    for(int i = 0; i < curFile; i++)
    {
        packetStream* strm = streamFrom(fileList[i]);
        if(strm)
        {
            streams[curStream] = strm;
            curStream++;
        }
    }
    
    double** codevMatrixSketch;
    double** codevMatrixNoSketch;
    
    
    if(do_CMS == 1)
    {
        /* Build the codeviance matrix estimated with CMS */
    
        codevMatrixSketch = autobuildCodevMatrix(streams, curStream,
            packlimit, 1, delta, epsilon, verbosity);
    }
    if(do_CMS == 1 && do_exact == 1)
    {
        /* 
        * Reset all streams here
        * This makes no sense if the stream is live, but anyway, we're not
        * calculating live streams...
        */
        for(int i = 0; i < curStream; i++)
        {
            resetStream(streams[i]);
        }
    }
    if(do_exact == 1)
    {
        /* Build the real codeviance matrix */
    
        codevMatrixNoSketch = autobuildCodevMatrix(streams, curStream,
            packlimit, 0, delta, epsilon, verbosity);
    }
    /* Output the result, in Python or in raw form */
    if(is_py == 1)
    {
        printf("from mpl_toolkits.mplot3d.axes3d import *\n"
        "import matplotlib.pyplot as libplot\n"
        "import matplotlib.mlab as ml\n"
        "from matplotlib import cm\n"
        "import numpy\n"
        "import os\n"
        "\n"
        "def autoplot(M, title):\n"
        "    n = len(M)\n"
        "    x = []\n"
        "    y = []\n"
        "    z = []\n"
        "    for i in range(n):\n"
        "        for j in range(n):\n"
        "            z.append(M[i][j])\n"
        "            x.append(i)\n"
        "            y.append(j)\n"
        "    xi = numpy.linspace(min(x), max(x))\n"
        "    yi = numpy.linspace(min(y), max(y))\n"
        "    X, Y = np.meshgrid(xi, yi)\n"
        "    Z = ml.griddata(x, y, z, xi, yi, interp='linear')\n"
        "    fig = libplot.figure()\n"
        "    ax = Axes3D(fig)\n"
        "    ax.plot_surface(X,Y,Z, rstride=1, cstride=1, cmap=cm.jet,"
        " linewidth=1, antialiased=True)\n"
        "    libplot.title(title)\n"
        "    libplot.draw()\n");
        if(do_CMS == 1) {
            printDoubleMatrix(codevMatrixSketch, curStream, curStream, 1, 
                          "cmsM");
            printf("autoplot(cmsM, \"CMS Estimated Codeviance\")\n");
        }
        if(do_exact == 1) {
            printDoubleMatrix(codevMatrixNoSketch, curStream, curStream, 1, 
                          "realM");
            printf("autoplot(realM, \"Real codeviance\")\n");
        }
        printf("libplot.show()\n");
    }
    else
    {
        if(do_CMS == 1)
        {
            printf("CMS-Estimated codeviance matrix:\n");
            printDoubleMatrix(codevMatrixSketch, curStream, curStream, 0, "");
        }
        if(do_exact == 1)
        {
            printf("Exact codeviance matrix:\n");
            printDoubleMatrix(codevMatrixNoSketch, curStream, curStream, 0, "");
        }
    }
    
    /* It's time for a little cleanup! */
    for(int i = 0; i < curStream; i++)
    {
        if(do_CMS == 1)
            free(codevMatrixSketch[i]);
        if(do_exact == 1)
            free(codevMatrixNoSketch[i]);
        closeStream(streams[i]);
    }
    free(analysis);
    free(analysis_params);
    gsl_rng_free(gener);
    if(do_exact == 1)
        free(codevMatrixNoSketch);
    if(do_CMS == 1)
        free(codevMatrixSketch);
    free(streams);
    free(fileList);
    return EXIT_SUCCESS;
}