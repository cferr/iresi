/* This file is part of CMSCD, Count-Min Sketch and CoDeviance
 * Calculator.
 * 
 * CMSCD is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * CMSCD is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with CMSCD.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Most of this code is an excerpt from Programming Abstractions 
 * in C by Eric Roberts.
 * It is available at:
 * http://www.cs.bu.edu/courses/Old/cs113/F97/roberts-source/PAC/13-Trees
 * /avltree.c
 * Given there is no license at all on this code, my changes on it
 * are licensed under the GPL.
 */

#include "avltree.h"

/*
 * Compares two strings.
 * Returns 1 if a > b, 0 if a == b, -1 if a < b.
 */

int StringCompare(char* a, char* b)
{
    int i = 0;
    while(a[i] && b[i])
    {
        if(a[i] > b[i])
            return 1;
        if(a[i] < b[i])
            return -1;
        i++;
    }
    if(a[i])
        return 1;
    if(b[i])
        return -1;
    return 0;
}


/*
 * Finds a subtree of the tree t, which root is labeled by key.
 * If there is no such node, returns NULL.
 */

treeT FindNode(treeT t, char* key)
{
    int sign;

    if (t == NULL) return (NULL);
    sign = StringCompare(key, t->key);
    if (sign == 0) return (t);
    if (sign < 0) {
        return (FindNode(t->left, key));
    } else {
        return (FindNode(t->right, key));
    }
}

/*
 * Calls InsertAVL and discards the result.
 */

void InsertNode(treeT *tptr, char* key, int* nodeCount, int updateCounts)
{
    (void) InsertAVL(tptr, key, nodeCount, updateCounts);
}

/*
 * Inserts a value in the AVL tree pointed to by tptr, and returns
 * the change in balance. Modifies tptr so that the resulting tree is AVL.
 * If the key already exists in the given tree:
 * - If updateCounts is set to 1, then its appearance counter is
 *   incremented.
 * - Otherwise, nothing happens.
 * If the key doesn't already exist, a node is created with 0 or 1
 * occurrence being counted, depending on updateCounts.
 * The integer pointed to by nodeCount reflects how many nodes the tree has.
 */

int InsertAVL(treeT *tptr, char* key, int* nodeCount, int updateCounts)
{
    treeT t;
    int sign, delta;

    t = *tptr;
    if (t == NULL) {
        t = (treeT)calloc(1, sizeof(nodeT));
        t->key = malloc((strlen(key) + 1)* sizeof(char));
        strcpy(t->key, key);
        t->bf = 0;
        t->nbOcc = 0;
        t->left = t->right = NULL;
        *tptr = t;
        if(nodeCount)
            *nodeCount += 1;
        return (+1);
    }
    
    sign = StringCompare(key, t->key);
    if (sign == 0) {
        if(updateCounts == 1)
            t->nbOcc += 1; // update the appearance counter
        return (0);
    }
    if (sign < 0) {
        delta = InsertAVL(&t->left, key, nodeCount, updateCounts);
        if (delta == 0) return (0);
        switch (t->bf) {
          // We may have inserted a node that unbalanced the tree.
          // If this is the case, fix that very unbalance.
          case +1: t->bf =  0; return (0);
          case  0: t->bf = -1; return (+1);
          case -1: FixLeftImbalance(tptr); return (0);
        }
    } else {
        delta = InsertAVL(&t->right, key, nodeCount, updateCounts);
        if (delta == 0) return (0);
        switch (t->bf) {
          case -1: t->bf =  0; return (0);
          case  0: t->bf = +1; return (+1);
          case +1: FixRightImbalance(tptr); return (0);
        }
    }
    // The following will never happen... but let's make the compiler happy!
    return (0);
}

/*
 * Fixes a tree that is too deep on its left branch.
 */

void FixLeftImbalance(treeT *tptr)
{
    treeT t, parent, child, *cptr;
    int oldBF;

    parent = *tptr;
    cptr = &parent->left;
    child = *cptr;
    if (child->bf != parent->bf) {
        oldBF = child->right->bf;
        RotateLeft(cptr);
        RotateRight(tptr);
        t = *tptr;
        t->bf = 0;
        switch (oldBF) {
          case -1: t->left->bf = 0; t->right->bf = +1; break;
          case  0: t->left->bf = t->right->bf = 0; break;
          case +1: t->left->bf = -1; t->right->bf = 0; break;
        }
    } else {
        RotateRight(tptr);
        t = *tptr;
        t->right->bf = t->bf = 0;
    }
}

/*
 * Rotates a tree so that the root is the old root's left child.
 */

void RotateLeft(treeT *tptr)
{
    treeT parent, child;

    parent = *tptr;
    child = parent->right;
    parent->right = child->left;
    child->left = parent;
    (*tptr) = child;
}

/*
 * Fixes a tree that is too deep on its right branch.
 */

void FixRightImbalance(treeT *tptr)
{
    treeT t, parent, child, *cptr;
    int oldBF;

    parent = *tptr;
    cptr = &parent->right;
    child = *cptr;
    if (child->bf != parent->bf) {
        oldBF = child->left->bf;
        RotateRight(cptr);
        RotateLeft(tptr);
        t = *tptr;
        t->bf = 0;
        switch (oldBF) {
          case -1: t->left->bf = 0; t->right->bf = +1; break;
          case  0: t->left->bf = t->right->bf = 0; break;
          case +1: t->left->bf = -1; t->right->bf = 0; break;
        }
    } else {
        RotateLeft(tptr);
        t = *tptr;
        t->left->bf = t->bf = 0;
    }
}

/*
 * Rotates a tree so that the root is the old root's right child.
 */

void RotateRight(treeT *tptr)
{
    treeT parent, child;

    parent = *tptr;
    child = parent->left;
    parent->left = child->right;
    child->right = parent;
    (*tptr) = child;
}

/*
 * Displays a tree in infix order.
 */

void DisplayTree(treeT t)
{
    if (t != NULL) {
        DisplayTree(t->left);
        printf("%s\n", t->key);
        DisplayTree(t->right);
    }
}

/*
 * Displays a tree, preserving its structure.
 */

void DisplayStructure(treeT t)
{
    RecDisplayStructure(t, 0, NULL);
}

void RecDisplayStructure(treeT t, int depth, char* label)
{
    if (t == NULL) return;
    printf("%*s", 3 * depth, "");
    if (label != NULL) printf("%s: ", label);
    printf("%s (%s%d)\n", t->key, (t->bf > 0) ? "+" : "", t->bf);
    RecDisplayStructure(t->left, depth + 1, "L");
    RecDisplayStructure(t->right, depth + 1, "R");
}

/*
 * Frees up the resources taken by an AVL tree.
 */

void freeTree(treeT tptr)
{
    if(tptr != NULL)
    {
        freeTree(tptr->left);
        freeTree(tptr->right);
        free(tptr->key);
        free(tptr);
    }
}