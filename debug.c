/* This file is part of CMSCD, Count-Min Sketch and CoDeviance
 * Calculator.
 * 
 * CMSCD is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * CMSCD is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with CMSCD.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "debug.h"

/*
 * Creates a random stream using Poisson's law.
 */
packetStream* poissonStream(gsl_rng* r,int numPackets, double mu)
{
    packet** stream = (packet**)malloc(numPackets * sizeof(packet*));
    
    for(int i = 0; i < numPackets; i++)
    {
        unsigned int num = gsl_ran_poisson(r, mu);
        char* origin = string_of_longint(num);
        packet* pack = (packet*)malloc(sizeof(packet));
        pack->origin = origin;
        
        stream[i] = pack;
    }
    
    packetStream* str = streamArray(stream, numPackets);
    return str;
}

/*
 * Creates a random stream using Gauss's law.
 */
packetStream* gaussianStream(gsl_rng* r,int numPackets, double sigma)
{
    packet** stream = (packet**)malloc(numPackets * sizeof(packet*));
    
    for(int i = 0; i < numPackets; i++)
    {
        double num = gsl_ran_gaussian_ziggurat(r, sigma);
        char* origin = (char*)calloc(17, sizeof(char)); //17-figure precision
        sprintf(origin, "%.3f", num);
        
        packet* pack = (packet*)malloc(sizeof(packet));
        pack->origin = origin;
        
        stream[i] = pack;
    }
    
    packetStream* str = streamArray(stream, numPackets);
    return str;
}

/*
 * Creates a random stream using a binomial law.
 */
packetStream* binomialStream(gsl_rng* r,int numPackets, double p, int n)
{
    packet** stream = (packet**)malloc(numPackets * sizeof(packet*));
    
    for(int i = 0; i < numPackets; i++)
    {
        unsigned int num = gsl_ran_binomial(r, p, n);
        char* origin = (char*)calloc(17, sizeof(char));
        sprintf(origin, "%d", num);
        
        packet* pack = (packet*)malloc(sizeof(packet));
        pack->origin = origin;
        
        stream[i] = pack;
    }
    
    packetStream* str = streamArray(stream, numPackets);
    return str;
}

/*
 * Creates a string given a long integer.
 */
char* string_of_longint(long int i)
{
    if(i == 0) {
        char* result = malloc(2 * sizeof(char));
        result[0] = '0';
        result[1] = 0;
        return result;
    }
    int numFigures = floor(log10((double)i)) + 1;
    char* result = calloc(numFigures + 1, sizeof(char));
    long int r = i;
    for(int j = 0; j < numFigures; j++)
    {
        result[numFigures-j-1] = (char)((r%10)+48);
        r = r / 10;
    }

    return result;
}

/*
 * Displays a packet stream on screen. TODO move this to utils.c
 */
void printPacketStream(packetStream* stream)
{
    packet* pack = getNextPacket(stream);
    while(pack != NULL)
    {
        printf("Packet : %s\n", pack->origin);
        pack = getNextPacket(stream);
    }
    resetStream(stream);
}

/*
 * Pretty-prints a matrix of doubles on screen. TODO move this to utils.c
 */
void printDoubleMatrix(double** mat, int max_x, int max_y, int is_python, char* python_name)
{
    if(is_python == 1)
        printf("%s = [", python_name);
    for(int x = 0; x < max_x; x++)
    {
        printf("[ ");
        for(int y = 0; y < max_y; y++)
        {
            printf("%f", mat[x][y]);
            if(y < max_y - 1)
                printf(", ");
            else
                printf(" ");
        }
        printf("]");
        if(is_python == 1 && x < max_x - 1)
            printf(", ");
        else if(is_python == 0)
            printf("\n");
    }
    if(is_python == 1)
        printf("]\n");
}
