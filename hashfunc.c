/* This file is part of CMSCD, Count-Min Sketch and CoDeviance
 * Calculator.
 * 
 * CMSCD is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * CMSCD is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with CMSCD.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hashfunc.h"


/*
 * Get n random bytes.
 */
char* getRandomBytes(int n)
{
    /* we should find a better way to keep the random
     * source open and not hassle all the other
     * programs that may be using it...
     */
    FILE* frandom = fopen("/dev/urandom", "r");
    
    /*
     * here the n+1 is there to get n effective bytes 
     * regardless of the the null terminator
     */
    char* buf = (char*)malloc((n + 1) * sizeof(char));
    
    fgets(buf, (n + 1), frandom);
    fclose(frandom);
    
    return buf;
}

/*
 * Build a single hash function from a random source.
 */
hashfunc* buildRandomHashFun(int cmsTableLen, int maxWL)
{
    int maxBytesSupported = ceil((float)(floor(log(cmsTableLen - 1) \
        / log(2)) + 1) / 8.0); //better use powers of 2, no ?
    hashfunc* hf = (hashfunc*)malloc(sizeof(hashfunc));
    
    if(maxBytesSupported > (int)sizeof(int))
    {
        fprintf(stderr, "FATAL : cannot handle table width above %d bits !\n",
                (int)sizeof(int) * 8);
        return NULL;
    }
    unsigned int** coeffs = (unsigned int**)malloc( \
        maxWL * sizeof(int*));
    
    /* build a, b coeffs such that a is even */
    for(int i = 0; i < maxWL; i++)
    {
        coeffs[i] = (unsigned int*)calloc(2, sizeof(int));
        char* seed_a = getRandomBytes((int)sizeof(int));
        char* seed_b = getRandomBytes(maxBytesSupported);
        
        for(int j = 0; j < (int)sizeof(int); j++)
            coeffs[i][0] += (unsigned int)seed_a[j] << 8*j;
        
        if(coeffs[i][0] & 0x01)
            coeffs[i][0] += 1;
        
        for(int j = 0; j < maxBytesSupported; j++)
            coeffs[i][1] += (unsigned int)seed_b[j] << 8*j;
        
        free(seed_a);
        free(seed_b);
    }
    
    hf->coeffs = coeffs;
    hf->maxWordLength = maxWL;
    hf->outputLength = maxBytesSupported;
    hf->maxOutVal = cmsTableLen;
    return hf;
}

/*
 * Hash a string given a hash function.
 */
unsigned int hash(hashfunc* hFunc, char* str, int len)
{
    /* 
     * Simple matrix product mod 2.
     * Note that if chars are ascii chars, the result never exceeds 128...
     */
    unsigned int result = 0;
    int maxbytes = min(len, hFunc->maxWordLength);
    for(int j = 0; j < maxbytes; j++)
    {
        int a = hFunc->coeffs[j][0];
        int b = hFunc->coeffs[j][1];
        result = result + ((unsigned int)(a * (unsigned int)str[j] + b)
            >> ((int)sizeof(int) - hFunc->outputLength));
    }
    
    return (result % (hFunc->maxOutVal));
    
}

/**
 * Build random hash functions.
 */
hashfunc** buildRandomFuncSet(int maxWordLength, int cmsTableLength, int funCount)
{
    // 2-universal hashing functions
    hashfunc** funcSet = (hashfunc**)calloc(funCount, sizeof(hashfunc));
    for(int i = 0; i < funCount; i++)
    {
        funcSet[i] = buildRandomHashFun(cmsTableLength, maxWordLength);
    }
    return funcSet;
}

/**
 * Free the resources used by a hash function.
 */
void freeHFunc(hashfunc* hFunc)
{
    for(int i = 0; i < hFunc->maxWordLength; i++)
    {
        free(hFunc->coeffs[i]);
    }
    free(hFunc->coeffs);
    free(hFunc);
}

/**
 * Free the resources used by a hash function set.
 */
void freeHFuncSet(hashfunc** funcSet, int funCount)
{
    for(int i = 0; i < funCount; i++)
    {
        freeHFunc(funcSet[i]);
    }
    free(funcSet);
}