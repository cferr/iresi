/* This file is part of CMSCD, Count-Min Sketch and CoDeviance
 * Calculator.
 * 
 * CMSCD is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * CMSCD is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with CMSCD.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cms.h"

/*
 * Estimates the minimum number of hash functions needed.
 */
int estimateCMSFNum(double delta)
{
    return ceil(log(1.0 / delta));
}

/*
 * Creates an empty CMS matrix.
 */
int** createCMSMatrix(int spaceWidth, int functionsCount)
{
    int** resultMatrix = (int**)malloc(functionsCount * sizeof(int*));
    
    for(int i = 0; i < functionsCount; i++)
    {
        resultMatrix[i] = (int*)calloc(spaceWidth, sizeof(int));
    }
    
    return resultMatrix;
}

/*
 * Updates a CMS vector by incrementing a hash's appearance count.
 */
void updateCMS(packet* pkt, int** cms, hashfunc** functions, 
               int functionsCount)
{
    
    for(int d = 0; d < functionsCount; d++)
    {
        int result = hash(functions[d], pkt->origin, strlen(pkt->origin));
        cms[d][result] = cms[d][result] + 1;
    }
}

/*
 * Performs the CMS given a stream and a set of functions.
 */
int** doCMS(int number, packetStream* stream, int cmsTableLength, 
            hashfunc** hFuncs, int numFunctions, int verbosity)
{
    /* init CMS */
    int** cms = createCMSMatrix(cmsTableLength, numFunctions);
    
    /* update cms while none of the packets are null 
     * or number is not reached */
    packet *curPacket = getNextPacket(stream);
    unsigned long count = 0;
    
    if(number == 0) //no factorization, but a significant gain on tests
    {
        while(curPacket)
        {
            updateCMS(curPacket, cms, hFuncs, numFunctions);
            freePacket(curPacket);
            curPacket = getNextPacket(stream);
            count++;
        }
    }
    else
    {
        while(curPacket && count < number)
        {
            updateCMS(curPacket, cms, hFuncs, numFunctions);
            freePacket(curPacket);
            curPacket = getNextPacket(stream);
            count++;
        }
    }
    
    if(verbosity >= 2)
        printf("%ld packets analyzed.\n", count);
    
    return cms;
}
