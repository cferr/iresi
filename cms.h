/* This file is part of CMSCD, Count-Min Sketch and CoDeviance
 * Calculator.
 * 
 * CMSCD is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * CMSCD is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with CMSCD.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CMS_H
#define CMS_H
#include <math.h>
#include "hashfunc.h"
#include "packet.h"
#include "datahandler.h"

int estimateCMSFNum(double delta);
int** createCMSMatrix(int spaceWidth, int functionsCount);
void updateCMS(packet* pkt, int** cms, hashfunc** functions, int functionsCount);
int** doCMS(int number, packetStream* stream, int cmsTableLength, hashfunc** hFuncs, int numFunctions, int verbosity);


#endif