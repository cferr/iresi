/* This file is part of CMSCD, Count-Min Sketch and CoDeviance
 * Calculator.
 * 
 * CMSCD is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * CMSCD is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with CMSCD.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EXACTANALYSIS_H
#define EXACTANALYSIS_H
#include "codeviance.h"
#include "avltree.h"
#include "packet.h"
#include "datahandler.h"

/*
 * We use AVL trees here : this reduces the overall complexity 
 * when inserting new packets (as nodes in the trees).
 */

int* buildFrequencyVector(treeT avlt, int nodeCount);
void recursiveFreqBuild(treeT avlt, int* curIndex, int* destArray);
double exactCodeviance(packetStream* ps1, packetStream* ps2, int count);
double exactSelfCodeviance(packetStream* ps, int count);

#endif