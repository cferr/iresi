/* This file is part of CMSCD, Count-Min Sketch and CoDeviance
 * Calculator.
 * 
 * CMSCD is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * CMSCD is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with CMSCD.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HASHFUNC_H
#define HASHFUNC_H
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "utils.h"

#define MAX_HOSTNAME_LENGTH 50

/*
 * Universal hash functions for words with bounded length.
 */
typedef struct hashfunc {
    unsigned int** coeffs;
    int maxWordLength; // max word length in BYTES
    int outputLength; // output length in BYTES
    int maxOutVal; // cms table length 
    // WARNING: width(cms table) must be divisible by 8 !!
} hashfunc;


char* getRandomBytes(int n);

hashfunc* buildRandomHashFun(int cmsTableLen, int maxWL);
hashfunc** buildRandomFuncSet(int maxWordLength, int cmsTableLength, int funCount);

unsigned int hash(hashfunc* hFunc, char* str, int len);

void freeHFunc(hashfunc* hFunc);
void freeHFuncSet(hashfunc** funcSet, int funCount);

#endif