#include "cmscd_params.h"

/*
 * Builds a linked list of parameters.
 */
analysis_param* addParam(analysis_param* existing, int intVal, double doubleVal)
{
    analysis_param* newParam = (analysis_param*) malloc(sizeof(analysis_param));
    newParam->int_val = intVal;
    newParam->double_val = doubleVal;
    newParam->next = existing;
    
    return newParam;
}

/*
 * Frees a parameter linked list.
 */
void freeParamStructure(analysis_param* prm)
{
    if(prm != NULL)
    {
        freeParamStructure(prm->next);
        free(prm);
    }
}