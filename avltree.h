/* This file is part of CMSCD, Count-Min Sketch and CoDeviance
 * Calculator.
 * 
 * CMSCD is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * CMSCD is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with CMSCD.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef AVLTREE_H
#define AVLTREE_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
 * AVL tree structure.
 */
typedef struct nodeT {
    char* key;
    struct nodeT *left, *right;
    int bf;
    int nbOcc;
} nodeT, *treeT;

treeT FindNode(treeT t, char* key);

void InsertNode(treeT *tptr, char* key, int* nodeCount, int updateCounts);
int InsertAVL(treeT *tptr, char* key, int* nodeCount, int updateCounts);

void FixLeftImbalance(treeT *tptr);
void FixRightImbalance(treeT *tptr);
void RotateLeft(treeT *tptr);
void RotateRight(treeT *tptr);
void DisplayTree(treeT t);
void DisplayStructure(treeT t);
void RecDisplayStructure(treeT t, int depth, char* label);
int StringCompare(char* a, char* b);

void freeTree(treeT tptr);

#endif