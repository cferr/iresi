/* This file is part of CMSCD, Count-Min Sketch and CoDeviance
 * Calculator.
 * 
 * CMSCD is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * CMSCD is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with CMSCD.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DATAHANDLER_H
#define DATAHANDLER_H
#include <stddef.h>

#ifdef __linux__
#define _GNU_SOURCE
#endif
#include <stdio.h>

#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "packet.h"
#include "utils.h"


typedef enum {stream_file, stream_array} stream_type;

/*
 * Generic structure to handle packet streams.
 */
typedef struct packetStream {
    FILE* handler; // for files, null if stream provides a full array
    packet** packetArray; // full array, null if stream provides a file
    packet* currentPacket;
    unsigned long currentPosition;
    unsigned long arrayLength;
    stream_type type;
} packetStream;

void closeStream(packetStream* ps);
void resetStream(packetStream* ps);

packetStream* streamFrom(char* location);
packetStream* streamArray(packet** array, int arrayLength);
packet* getNextPacket(packetStream* stream);

packet* getNextPacketFile(packetStream* stream);
packet* getNextPacketArray(packetStream* stream);

#endif