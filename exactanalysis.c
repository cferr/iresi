/* This file is part of CMSCD, Count-Min Sketch and CoDeviance
 * Calculator.
 * 
 * CMSCD is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * CMSCD is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with CMSCD.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "exactanalysis.h"

/*
 * Builds a frequency vector for a given tree t.
 */
int* buildFrequencyVector(treeT avlt, int nodeCount)
{
    /*
     * First of all, allocate as much memory as needed.
     * Using an avl tree minimizes this !
     */
    
    int* freqVect = (int*)calloc(nodeCount, sizeof(int));
    
    int curIndex = 0;
    recursiveFreqBuild(avlt, &curIndex, freqVect);
    
    return freqVect;
}

/*
 * Recursive function to browse a tre for building its frequency vector.
 */
void recursiveFreqBuild(treeT avlt, int* curIndex, int* destArray)
{
    /* Simple as hell : infix browsing ! */
    if(avlt != NULL)
    {
        recursiveFreqBuild(avlt->left, curIndex, destArray);
        
        destArray[*curIndex] = avlt->nbOcc;
        *curIndex += 1;
        
        recursiveFreqBuild(avlt->right, curIndex, destArray);
    }
}

/*
 * Calculates the exact codeviance of two given streams.
 * Note that we use AVL trees to avoid an O(n^2) search and insertion
 * as both functions are more or less equally used.
 */
double exactCodeviance(packetStream* ps1, packetStream* ps2, int count)
{    
    treeT st1 = NULL; // search tree for stream 1
    treeT st2 = NULL; // search tree for stream 2
    
    int nodeCount1 = 0;
    int nodeCount2 = 0; //they should be the same
    packet* p = NULL;
    
    do
    {
        if(p != NULL)
        {
            InsertNode(&st1, p->origin, &nodeCount1, 1);
            /* Don't update counts for stream 2 ! */
            InsertNode(&st2, p->origin, &nodeCount2, 0); 
            freePacket(p); 
        }
        p = getNextPacket(ps1);
    } while(p != NULL && nodeCount1 < count);
    
    int secCount = 0;
    
    do
    {
        if(p != NULL)
        {
            /* The roles shall be inverted... */
            InsertNode(&st1, p->origin, &nodeCount1, 0); 
            InsertNode(&st2, p->origin, &nodeCount2, 1); 
            freePacket(p);
            secCount++;
        }
        p = getNextPacket(ps2);
    } while(p != NULL && secCount < count);

    
    /* Build frequency vectors, calculate their codeviance. */
    
    int* freqVect1 = buildFrequencyVector(st1, nodeCount1);
    int* freqVect2 = buildFrequencyVector(st2, nodeCount2);
    
    double codev = codeviance2(freqVect1, freqVect2, nodeCount1);
    
    /* Free all the structures used there. */
    
    free(freqVect1);
    free(freqVect2);
    freeTree(st1);
    freeTree(st2);
    
    return codev;
}

/* 
 * Calculates the exact codeviance for a given stream
 * versus itself. This can't be done with the previous
 * function, that would just break off when looking
 * for the second stream without rewinding it.
 * 
 * Note that rewinding isn't always possible : this
 * one is to prefer in that case.
 */

double exactSelfCodeviance(packetStream* ps, int count)
{    
    treeT st1 = NULL; 
    treeT st2 = NULL;
    
    int nodeCount1 = 0;
    int nodeCount2 = 0; //they should be the same
    packet* p = NULL;
    
    do
    {
        if(p != NULL)
        {
            InsertNode(&st1, p->origin, &nodeCount1, 1);
            InsertNode(&st2, p->origin, &nodeCount2, 1);
            freePacket(p);
        }
        p = getNextPacket(ps);
    } while(p != NULL && nodeCount1 < count);
    
    int* freqVect1 = buildFrequencyVector(st1, nodeCount1);
    int* freqVect2 = buildFrequencyVector(st2, nodeCount2);
    
    double codev = codeviance2(freqVect1, freqVect2, nodeCount1);
    free(freqVect1);
    free(freqVect2);
    freeTree(st1);
    freeTree(st2);
    
    return codev;
}