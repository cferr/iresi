CC = gcc
OBJECTS = packet.o datahandler.o hashfunc.o cms.o utils.o codeviance.o avltree.o exactanalysis.o main.o debug.o cmscd.o cmscd_params.o
LIBS = -lm -lgsl -lgslcblas
CFLAGS = -Wall -std=c99
SRCS = $(OBJECTS:%.o=%.c)
EXECUTABLE = cmscd
LDFLAGS = $(LIBS)
REPORT = rap/rap.tex

all: $(SRCS) $(EXECUTABLE)

full: all doc report

$(EXECUTABLE): $(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

$(SRCS):
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm -f *.o *.out *.aux *.toc *.log

distclean:
	rm -f *.o *.out *.aux *.toc *.log *.pdf $(EXECUTABLE)
	rm -rf doc

doc:
	doxygen Doxyfile

report:
	pdflatex $(REPORT)