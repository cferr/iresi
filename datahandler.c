/* This file is part of CMSCD, Count-Min Sketch and CoDeviance
 * Calculator.
 * 
 * CMSCD is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * CMSCD is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with CMSCD.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "datahandler.h"


/*
 * Creates a stream from a file.
 */
packetStream* streamFrom(char* location)
{
    errno = 0;
    FILE* fh = fopen(location, "r");
    if(fh == NULL) {
        char* errmsg = NULL;
#ifdef _GNU_SOURCE
        asprintf(&errmsg, "Can't open file %s: %s", location, strerror (errno));
        printErr(progname, errmsg);
#endif
        return NULL;
    }
    
    packetStream* retStream = (packetStream*)malloc(sizeof(packetStream));

    retStream->handler = fh;
    retStream->currentPacket = NULL;
    retStream->currentPosition = 0;
    retStream->type = stream_file;
    retStream->packetArray = NULL;
    
    fseek(fh, 0, SEEK_SET);
    
    return retStream;
}

/*
 * Creates a stream from a given array of packets.
 */
packetStream* streamArray(packet** array, int arrayLength)
{
    packetStream* retStream = (packetStream*)malloc(sizeof(packetStream));
    retStream->type = stream_array;
    retStream->packetArray = array;
    retStream->currentPosition = 0;
    retStream->arrayLength = arrayLength;
    
    retStream->currentPacket = NULL;
    retStream->handler = NULL;
    
    return retStream;
}

/*
 * Rewinds a stream.
 */
void resetStream(packetStream* ps)
{
    switch(ps->type)
    {
        case stream_array:
            ps->currentPosition = 0;
            break;
        case stream_file:
            rewind(ps->handler);
            ps->currentPosition = 0;
            break;
        default:
            break;
    }
}

/*
 * Frees up the structures we've been using.
 */
void closeStream(packetStream* ps)
{
    // is there something more to be done ?
    if(ps -> currentPacket != NULL)
        freePacket(ps->currentPacket);
    
    if(ps->handler != NULL)
        fclose(ps->handler);
    
    if(ps->packetArray != NULL)
    {
        for(unsigned long i = 0; i < ps->arrayLength; i++)
        {
            freePacket(ps->packetArray[i]);
        }
        free(ps->packetArray);
    }
    free(ps);
}


/*
 * Returns the next packet in the stream, or NULL if there's none.
 */
packet* getNextPacket(packetStream* stream)
{
    packet* result;
    switch(stream->type)
    {
        case stream_file:
            result = getNextPacketFile(stream);
            break;
        case stream_array:
            result = getNextPacketArray(stream);
            break;
        default:
            result = NULL;
            break;
    }
    return result;
}


/*
 * Gets the next packet by reading a line from a file.
 */
packet* getNextPacketFile(packetStream* stream)
{
    int lineLen;
    char* line = getuline(stream->handler, &lineLen);
    if(lineLen == 0)
        return NULL;
        
    packet* pack = (packet*)malloc(sizeof(packet));
    stream->currentPosition += lineLen + 1;
    pack->origin = line;

    return pack;
}

/*
 * Gets the next packet by reading the next element in the array.
 */
packet* getNextPacketArray(packetStream* stream)
{
    if(stream->currentPosition >= stream->arrayLength)
        return NULL;
    
    packet* pack = (packet*)malloc(sizeof(packet));
    packet* ours = stream->packetArray[stream->currentPosition];

    int len = strlen(ours->origin);
    pack->origin = (char*)calloc(len + 1, sizeof(char));
    strncpy(pack->origin, ours->origin, len);
    
    stream->currentPosition += 1;
    
    return pack;
}