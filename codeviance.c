/* This file is part of CMSCD, Count-Min Sketch and CoDeviance
 * Calculator.
 * 
 * CMSCD is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * CMSCD is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with CMSCD.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "codeviance.h"

/*
 * Calculates the average value of an int array.
 */
double averageIntArray(int* values, int count)
{
    unsigned long int sum = 0;
    for(int i = 0; i < count; i++)
        sum += values[i];
    
    double result = ((double)sum) / ((double)count);

    return result;
}


/* 
 * Calculates the codeviance of two vectors.
 */
double codeviance2(int* vect1, int* vect2, int count)
{
    double avg1 = averageIntArray(vect1, count);
    double avg2 = averageIntArray(vect1, count);
        
    unsigned long int sum = 0;
    for(int i = 0; i < count; i++)
    {
        sum += vect1[i] * vect2[i];
    }
    double partialResult = (double)sum / (double)count;
    
    double result = partialResult - avg1 * avg2;
    
    return result;
}

/*
 * Calculates the cross-codeviances of all vectors from two
 * CMS matrices, then returns the minimum value.
 */
double minCodev(int** cms1, int** cms2, int nbFunctions, int cmsTableWidth)
{
    double codev = codeviance2(cms1[0], cms2[0], cmsTableWidth);
    for(int i = 1; i < nbFunctions; i++)
    {
        double localCodev = codeviance2(cms1[i], cms2[i], cmsTableWidth);
        codev = fmin(codev, localCodev);
    }
    
    return codev;
}