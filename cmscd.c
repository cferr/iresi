/* This file is part of CMSCD, Count-Min Sketch and CoDeviance
 * Calculator.
 * 
 * CMSCD is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * CMSCD is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with CMSCD.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cmscd.h"

/*
 * Calculates the codeviance of two packet streams.
 */
double genericCodev2(packetStream* strm1, packetStream* strm2, 
                     int withCMS, hashfunc** hFuncs, int numPackets, 
                     int cmsTableLength, int numFunctions, int verbosity)
{
    double codev;
    if(withCMS == 1)
    {
        int** cms1 = doCMS(numPackets, strm1, cmsTableLength, hFuncs,
                           numFunctions, verbosity);
        int** cms2 = doCMS(numPackets, strm2, cmsTableLength, hFuncs,
                           numFunctions, verbosity);
        codev = minCodev(cms1, cms2, numFunctions, cmsTableLength);
        
        /* Avoid loss here ! */
        for(int i = 0; i < numFunctions; i++)
        {
            free(cms1[i]);
            free(cms2[i]);
            
        }
        free(cms1);
        free(cms2);
        
    }
    else 
    {
        codev = exactCodeviance(strm1, strm2, numPackets);
    }
    return codev;
}

/*
 * Calculates the codeviance of a stream against itself.
 */
double genericSelfCodev(packetStream* strm, int withCMS, hashfunc** hFuncs,
                        int numPackets, int cmsTableLength, 
                        int numFunctions, int verbosity)
{
    double codev;
    if(withCMS == 1)
    {
        int** cms = doCMS(numPackets, strm, cmsTableLength, hFuncs,
                          numFunctions, verbosity);
        codev = minCodev(cms, cms, numFunctions, cmsTableLength);
        
        /* Avoid loss here ! */
        for(int i = 0; i < numFunctions; i++)
        {
            free(cms[i]);
        }
        free(cms);
        
    }
    else 
    {
        codev = exactSelfCodeviance(strm, numPackets);
    }
    return codev;
}

/*
 * Generates the codeviance matrix for streamCount streams.
 * useSketch determines whether to use CMS or to do an exact calculation.
 */
double** autobuildCodevMatrix(packetStream** streamList, int streamCount,
                              int packetCount, int useSketch, double delta, 
                              double epsilon, int verbosity)
{

    double e = exp(0);
    
    int cmsTableLength = (int)(e / epsilon);
    int maxHNLen = MAX_HOSTNAME_LENGTH; // the hash function needs them...
    int numFunctions = estimateCMSFNum(delta);
    
    hashfunc** hFuncs = NULL;
    int*** cmsVectors = NULL;
    
    if(useSketch == 1) 
    {
        if(numFunctions <= 0) {
            printErr(progname, "Delta should be strictly less than 1. "
            "Not calculating CMS.");
            return NULL;
        }
        if(verbosity >= 2)
            printf("Generating %d functions... \n", numFunctions);
        hFuncs = buildRandomFuncSet(maxHNLen, cmsTableLength, numFunctions);
        cmsVectors = (int***)malloc(streamCount * sizeof(int**));
        for(int i = 0; i < streamCount; i++)
        {
            if(verbosity >= 2)
                printf("Generating CMS for stream %d...\n", i);
            cmsVectors[i] = doCMS(packetCount, streamList[i], cmsTableLength,
                                  hFuncs, numFunctions, verbosity);
            resetStream(streamList[i]);
        }
    }
    double** codevMatrix = (double**)malloc(streamCount * sizeof(double*));
    
    double codev;
    for(int i = 0; i < streamCount; i++)
    {
        codevMatrix[i] = (double*)calloc(streamCount, sizeof(double));
        /* Calculate all the coeffs, but the diagonal ones */
        for(int j = i + 1; j < streamCount; j++) 
        {
            if(useSketch == 1)
                codev = minCodev(cmsVectors[i], cmsVectors[j], numFunctions,
                                 cmsTableLength);
            else 
            {
                if(verbosity >= 2)
                    printf("Calculating exact codeviance for stream %d "
                    "against stream %d\n", i, j);
                codev = exactCodeviance(streamList[i], streamList[j], 
                                        packetCount);
                resetStream(streamList[i]);
                resetStream(streamList[j]);
            }
            codevMatrix[i][j] = codev;
        }
    }
    
    /* Fill the diagonal */
    for(int i = 0; i < streamCount; i++)
    {
        if(useSketch == 1)
            codev = minCodev(cmsVectors[i], cmsVectors[i], numFunctions,
                             cmsTableLength);
        else 
        {
            codev = exactSelfCodeviance(streamList[i], packetCount);
            resetStream(streamList[i]);
        }
        codevMatrix[i][i] = codev;
    }
    
    /* Symmetrize the matrix */
    for(int i = 0; i < streamCount; i++)
    {
        for(int j = i + 1; j < streamCount; j++) 
        {
            codevMatrix[j][i] = codevMatrix[i][j];
        }
    }
    
    if(useSketch == 1) {
        freeHFuncSet(hFuncs, numFunctions);
        for(int i = 0; i < streamCount; i++)
        {
            for(int j = 0; j < numFunctions; j++)
            {
                free(cmsVectors[i][j]);
            }
            free(cmsVectors[i]);
        }
        free(cmsVectors);
    }
    return codevMatrix;
}
